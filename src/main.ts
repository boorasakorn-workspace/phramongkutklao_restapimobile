import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(5002);
  const options = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    // "preflightContinue": false,
    // "optionsSuccessStatus": 204,
    "allowedHeaders": "*,Authorization",
    "exposedHeaders": "Authorization"
    // "credentials": true
  }
  app.enableCors(options);
}
bootstrap();
