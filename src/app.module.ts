import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RegisterlistqueueModule } from './registerlistqueue/registerlistqueue.module';
import { QrcodequeueModule } from './qrcodequeue/qrcodequeue.module';
import { PharcashModule } from './pharcash/pharcash.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: '',
    port: 5432,
    username: '',
    password: '',
    database: '',
    entities: [__dirname + '/**/queue.entity{.ts,.js}'],
    synchronize: false,
  }),TypeOrmModule.forRoot({
    name: '',
    type: '',
    host: '',
    port: ,
    username: '',
    password: '',
    database: '',
    entities: [__dirname + '/**/pharcash.entity{.ts,.js}'],
    synchronize: false,
  }), RegisterlistqueueModule, QrcodequeueModule,PharcashModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection){}
}