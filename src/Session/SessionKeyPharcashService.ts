import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class SessionKeyPharcashService {
  private sessionSecret: string;
  private sessionPrivateKey: string;

  constructor(
    // private readonly configService: ConfigService
  ) {
    // this.sessionSecret = configService.get('session').secret;
    // this.sessionPrivateKey = configService.get('session').privateKey;
  }

  public generateSessionId(tokenId: string, exp: number): string {
    // signature => hash(tokeId:exp:secret)
    // hash-based token => tokenId:exp:signature
    const signature = this.encryptHmac(`${tokenId}:${exp}:${'LPVaNs4vc9PFvEwB'}`);//const signature = this.encryptHmac(`${tokenId}:${exp}:${'8EUPPyG2BZM6ZgEx'}`);
    let token = `${tokenId}:${exp}:${signature}`;
    token = Buffer.from(token).toString('base64');
    return token;
  }

  public encryptHmac(str: string): string {
    const hmac = crypto.createHmac('sha512', "hyLCQ4BmzwLAEGqhDZF2I6IKMBq0USZIGyD27F1pC7E");//const hmac = crypto.createHmac('sha512', "cjDl5TRtMZLePCoWDVELW4Aef_UjduuSpin8KpnoVNw");
    return hmac.update(Buffer.from(str, 'utf-8')).digest('base64');
  }

  public extractSessionId(sessionId: string): { signature: string; tokenId: string; exp: number } {
    const buff = Buffer.from(sessionId, 'base64');
    const text = buff.toString('ascii');
    const [tokenId, exp, signature] = text.split(':');

    return {
      signature,
      tokenId,
      exp: parseInt(exp, 10),
    };
  }

  public validateSession(signature: string, tokenId: string, exp: number): boolean {
    const computedHash = this.encryptHmac(`${tokenId}:${exp}:${'LPVaNs4vc9PFvEwB'}`);// const computedHash = this.encryptHmac(`${tokenId}:${exp}:${'8EUPPyG2BZM6ZgEx'}`);
    return signature === computedHash;
  }

}
