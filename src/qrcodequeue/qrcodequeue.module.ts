import { Module } from '@nestjs/common';
import { QrcodequeueController } from './qrcodequeue.controller';
import { SessionKeyService} from '../Session/SessionKeyService';
import { SessionKeyPharcashService } from '../Session/SessionKeyPharcashService'; 
import { QrcodequeuePharcashController } from '../qrcodequeue/qrcodequeuepharcash.controller';

@Module({
  providers: [SessionKeyService,SessionKeyPharcashService],
  controllers: [QrcodequeueController,QrcodequeuePharcashController]
})
export class QrcodequeueModule {}
