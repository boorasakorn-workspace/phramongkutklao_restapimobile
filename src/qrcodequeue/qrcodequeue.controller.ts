import { Controller, Res, Body, Post, HttpStatus } from '@nestjs/common';
import { SessionKeyService } from '../Session/SessionKeyService';
const https = require('https');
import axios from 'axios';
@Controller('dev/generatesession')
export class QrcodequeueController {
    constructor(
        private readonly seesionkey: SessionKeyService
    ) { }

    @Post()
    createseesionkey(@Body() body, @Res() res) {
        if (body.pid === "") {
            res.status(HttpStatus.NOT_FOUND).send({
                statusid: "0",
                status: "pid emty ",
                urlqrcode: ""
            });
        }
        else if(body.pid === undefined){
            res.status(HttpStatus.NOT_FOUND).send({
                statusid: "0",
                status: "pid undefined ",
                urlqrcode: ""
            });
        }
        else{
            // const agent = new https.Agent({
            //     rejectUnauthorized: false
            // })
            // const instance = axios.create({
            //     httpsAgent: new https.Agent({  
            //       rejectUnauthorized: false
            //     })
            //   });
            // console.log(this.seesionkey.generateSessionId(body.pid, 30));
            let oldurl = `https://pmk.healthyflow.io/journey/${body.patientuid}?sessionKey=` + this.seesionkey.generateSessionId(body.pid, 30);
            axios.post('https://flow-tiny-url.pmk.local/tiny-url/api/v1/tiny',{"longUrl": oldurl},{
                httpsAgent: new https.Agent({  
                  rejectUnauthorized: false
                })
              }
            ).then(function (res_) {
                res.status(HttpStatus.OK).send({
                    statusid: "1",
                    status: "Success",
                    urlqrcode: "https://pmk.healthyflow.io/tiny/"+ res_.data.tinyUrl
                });
            },error => {
                console.log(error);
                res.status(HttpStatus.OK).send({
                    statusid: "1",
                    status: "Success",
                    urlqrcode: "https://pmk.healthyflow.io/tiny/"
                });
            }) 

           

            
        }
    }
}
