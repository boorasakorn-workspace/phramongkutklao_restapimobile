export class queuedata {
  hn: string;
  pid: string;
  patientName: string;
  activities: Array<activities> = [];
}

export class activities {
  id: string;
  serviceType: string;
  qType: string;
  qSubType: string;
  qNumber: string;
  location: string;
  channel: string;
  case: Number;
  caseMessage: string;
  caseDescription: string;
  serviceUnitName: string;
  instructions: Array<dataInstructions> = [];

}

export class dataInstructions {
  IconPath: string;
  title: string;
}