import { Controller, Post, Body, Delete, Put, Param, Query, Header, Req, Res, HttpStatus, Get } from '@nestjs/common';
import { RegisterlistqueueService } from './registerlistqueue.service';
import { queuedata, activities, dataInstructions } from './queue';
import { SessionKeyService } from '../Session/SessionKeyService';
import { async } from 'rxjs/internal/scheduler/async';
import axios from 'axios';
@Controller('api/q')
export class RegisterlistqueueController {
    constructor(
        private readonly registerq: RegisterlistqueueService,
        private readonly seesionkeey: SessionKeyService
    ) { }

    @Get()
    getpatientqueue(@Res() res, @Query() qr, @Param() pram) {
        //console.log(pram.id);
        //console.log(qr.sessionKey);
        //console.log(this.seesionkeey.generateSessionId("1259700092522", 30));
        //console.log(this.seesionkeey.extractSessionId("MTI1OTcwMDA5MjUyMjozMDpEMXlzZ2tua3VqZnlJamllT0ZxZ0kwZURKQlZ0V2tXSjRzK3dIZEM5b0ZlTDErQ2dpVnpieWRyV3VTSmZ3azkvSE5SR1JTL2NtNWcwOFZ2eDBLNjJDUT09"));
        let sesionkey = this.seesionkeey.extractSessionId(qr.sessionKey);
        let validate = this.seesionkeey.validateSession(sesionkey.signature, sesionkey.tokenId, 30);



        if (validate !== false) {


            let activities_: Array<activities> = [];

            let data: queuedata = new queuedata();
            let patientqueue;
            let dataInstructions_: Array<dataInstructions> = [];
            this.registerq.findpatientprocess(sesionkey.tokenId).then(xx => {
                xx.forEach(e => {
                    let dI: dataInstructions = new dataInstructions();
                    dI.IconPath = e.iconpath;
                    dI.title = e.description;
                    dataInstructions_.push(dI);
                });

            });



            this.registerq.findpatient_queue(sesionkey.tokenId).then(x => {
                
                if (x.length > 0) {

                    x.forEach(em => {

                        let ac: activities = new activities();

                        ac.qNumber = em.queueno;
                        if (em.groupprocessuid === "1") {
                            ac.qType = "คิวทำประวัติ";
                        }
                        else { ac.qType = "คิวเปิดสิทธิ"; }
                        ac.id = em.patientuid;
                        ac.serviceType = "registration";
                        ac.location = "เวชระเบียน";
                        ac.case = this.CheckcaseInQueue(em.lastworklist, em.groupprocessuid, x.length, em.count_patient_newhn, em.count_patient_payor, em.worklistgroupuid, em.counter).case;
                        ac.caseMessage = this.CheckcaseInQueue(em.lastworklist, em.groupprocessuid, x.length, em.count_patient_newhn, em.count_patient_payor, em.worklistgroupuid, em.counter).caseMessage;
                        ac.caseDescription = this.CheckcaseInQueue(em.lastworklist, em.groupprocessuid, x.length, em.count_patient_newhn, em.count_patient_payor, em.worklistgroupuid, em.counter).caseDescription;
                        ac.qSubType = em.payorname;
                        ac.channel = em.allcounter;
                        ac.instructions = dataInstructions_;
                        ac.serviceUnitName = "เวชระเบียน";
                        data.activities.push(ac);
                        data.hn = em.hn;
                        data.patientName = em.patientname;
                        data.pid = em.idcard;


                    });
                    //console.log(x);
                    res.status(HttpStatus.OK).send(data);
                }
                else {
                    res.status(HttpStatus.NOT_FOUND).send({ statuscode: HttpStatus.NOT_FOUND });
                }


            });
        } else {
            res.status(HttpStatus.UNAUTHORIZED).send({ statuscode: HttpStatus.UNAUTHORIZED });
        }

    }


    public CheckcaseInQueue(processcontrolcurrent: string, groupservice: string, dataleaght: number, countnewhn: string, countpayor: string, worklistgroup: number, counter_: string)
        : { case: Number, caseMessage: string, caseDescription: string } {
        if (processcontrolcurrent != null) {
            if (processcontrolcurrent.search("Hold") != -1) {
                if (dataleaght == 2 || dataleaght > 2) {

                    if (groupservice == "2") {
                        return this.checkDesc(7, groupservice, countpayor, "");
                    }
                    else if (groupservice == "1") {
                        return this.checkDesc(5, groupservice, countnewhn, "");
                    }
                }
                else {

                    return this.checkDesc(5, groupservice, countpayor, "");

                }
            }
            else {
                switch (processcontrolcurrent) {
                    case "กรอกเอกสารรับบริการ": {
                        if (worklistgroup == 2 || worklistgroup == 5) {
                            console.log(worklistgroup)
                            console.log(countpayor)
                            if (groupservice == "2") {
                                return this.checkDesc(5, groupservice, countpayor, "");
                            }
                        } else {
                            return this.checkDesc(6, groupservice, "0", "");
                        }
                        break;
                    }
                    case "คัดกรองแล้ว": {

                        if (dataleaght == 2 || dataleaght > 2) {

                            if (groupservice == "2") {
                                return this.checkDesc(7, groupservice, countpayor, "");
                            }
                            else if (groupservice == "1") {
                                return this.checkDesc(5, groupservice, countnewhn, "");
                            }
                        }
                        else {

                            if (groupservice == "1") {
                                return this.checkDesc(5, groupservice, countnewhn, "");
                            }
                            else if (groupservice == "2") {
                                return this.checkDesc(5, groupservice, countpayor, "");
                            }
                            // if (groupservice == "2") {
                            //     return this.checkDesc(5, groupservice);
                            // }
                            // else if (groupservice == "1") {
                            //     return this.checkDesc(5, groupservice);
                            // }
                        }

                        break;
                    }
                    case "เปิดสิทธิ": {

                        if (dataleaght == 2 || dataleaght > 2) {

                            if (groupservice == "2") {
                                return this.checkDesc(7, groupservice, countpayor, "");
                            }
                            else if (groupservice == "1") {
                                return this.checkDesc(5, groupservice, countnewhn, "");
                            }
                        }
                        else {

                            if (groupservice == "1") {
                                return this.checkDesc(5, groupservice, countnewhn, "");
                            }
                            else if (groupservice == "2") {
                                return this.checkDesc(5, groupservice, countpayor, "");
                            }
                            // if (groupservice == "2") {
                            //     return this.checkDesc(5, groupservice);
                            // }
                            // else if (groupservice == "1") {
                            //     return this.checkDesc(5, groupservice);
                            // }
                        }

                        break;
                    }
                    case "เรียกคิว (Call) ทำประวัติ": {
                        if (dataleaght == 2 || dataleaght > 2) {

                            if (groupservice == "2") {
                                return this.checkDesc(7, groupservice, countpayor, "");
                            }
                            else if (groupservice == "1") {
                                return this.checkDesc(4, "1", "0", counter_);
                            }
                        }
                        else {
                            return this.checkDesc(4, "1", "0", counter_);
                        }

                        break;
                    }
                    case "เรียกคิว (Call) เปิดสิทธิ": {
                        if (dataleaght == 2 || dataleaght > 2) {

                            if (groupservice == "2") {
                                return this.checkDesc(4, "2", "0", counter_);
                            }
                            else if (groupservice == "1") {
                                //return this.checkDesc(4, "1", "0", counter_);
                                return this.checkDesc(2, "1", countpayor, "");
                            }
                        }
                        else {
                            return this.checkDesc(4, "2", "0", counter_);
                        }

                        
                        break;
                    }
                    case "ทำประวัติแล้ว": {
                        if (groupservice == "1") {
                            return this.checkDesc(2, groupservice, countnewhn, "");
                        }
                        else if (groupservice == "2") {
                            return this.checkDesc(5, groupservice, countpayor, "");
                        }
                        break;
                    }
                    case "เปิดสิทธิแล้ว": {
                        return this.checkDesc(2, "2", "0", "");
                        break;
                    }
                    case "ยกเลิกคิว": {
                        return this.checkDesc(1, groupservice, "0", "");
                        break;
                    }
                    case "ยกเลิกคิว": {
                        return this.checkDesc(1, groupservice, "0", "");
                        break;
                    }
                    case "ทำประวัติและเปิดสิทธิแล้ว": {
                        return this.checkDesc(8, groupservice, "0", "");
                        break;
                    }
                    default: {
                        return this.checkDesc(0, groupservice, "0", "");
                        break;
                    }
                }
            }
        }
    }


    public checkDesc(case_: number, groupservice: string, countq: string, counter: string): { case: Number, caseMessage: string, caseDescription: string } {
        if (case_ !== undefined) {
            if (groupservice == "2") {
                switch (case_) {
                    case 1: {

                        return {
                            case: 1,
                            caseMessage: "ยกเลิก",
                            caseDescription: "สอบถามเจ้าหน้าที่ได้ที่เบอร์ 02-xxx-xxx"
                        };
                        break;
                    }
                    case 2: {
                        return {
                            case: 2,
                            caseMessage: "เสร็จสิ้น",
                            caseDescription: "ติดต่อเปิดสิทธิ, ติดต่อ OPD"
                        };
                        break;
                    }
                    case 4: {
                        if (counter == null) { counter = "-" }
                        return {
                            case: 4,
                            caseMessage: "ถึงคิวของคุณแล้ว",
                            caseDescription: `ติดดต่อเจ้าหน้าที่ช่องบริการ ${counter} `
                        };
                        break;
                    }
                    case 5: {

                        return {
                            case: 5,
                            caseMessage: ` ยังไม่ถึงคิว : รออีก ${countq} คิว`,
                            caseDescription: "รอเรียกคิว"
                        };
                        break;
                    }
                    case 6: {
                        return {
                            case: 6,
                            caseMessage: "รอคัดกรอง",
                            caseDescription: "กรอกเอกสารขอรับบริการ และติดต่อจุดคัดกรอง"
                        };
                        break;
                    }
                    case 7: {
                        return {
                            case: 6,
                            caseMessage: "รอการทำประวัติผู้ป่วย",
                            caseDescription: "กรอกเอกสารขอรับบริการ และติดต่อจุดคัดกรอง"
                        };
                        break;
                    }
                    case 8: {
                        return {
                            case: 2,
                            caseMessage: "เสร็จสิ้น",
                            caseDescription: "ติดต่อ OPD"
                        };
                        break;
                    }
                    default: {
                        return {
                            case: 0,
                            caseMessage: "-",
                            caseDescription: "-"
                        }
                        break;
                    }
                }
            }
            else {
                switch (case_) {
                    case 1: {

                        return {
                            case: 1,
                            caseMessage: "ยกเลิก",
                            caseDescription: "สอบถามเจ้าหน้าที่ได้ที่เบอร์ 02-xxx-xxx"
                        };
                        break;
                    }
                    case 2: {
                        return {
                            case: 2,
                            caseMessage: "เสร็จสิ้น",
                            caseDescription: "ติดต่อเปิดสิทธิ (กรณีมีคิวเปิดสิทธิ), ติดต่อ OPD"
                        };
                        break;
                    }
                    case 4: {
                        if (counter == null) { counter = "-" }
                        return {
                            case: 4,
                            caseMessage: "ถึงคิวของคุณแล้ว",
                            caseDescription: `ติดดต่อเจ้าหน้าที่ช่องบริการ ${counter}`
                        };
                        break;
                    }
                    case 5: {
                        return {
                            case: 5,
                            caseMessage: `ยังไม่ถึงคิว : รออีก ${countq} คิว`,
                            caseDescription: "รอเรียกคิว"
                        };
                        break;
                    }
                    case 6: {
                        return {
                            case: 6,
                            caseMessage: "รอคัดกรอง",
                            caseDescription: "กรอกเอกสารขอรับบริการ และติดต่อจุดคัดกรอง"
                        };
                        break;
                    }
                    case 6: {
                        return {
                            case: 6,
                            caseMessage: "รอการทำประวัติผู้ป่วย",
                            caseDescription: "ทำประวัติผู้ป่วย และรอเรียกคิวเปิดสิทธิ"
                        };
                        break;
                    }
                    case 8: {
                        return {
                            case: 2,
                            caseMessage: "เสร็จสิ้น",
                            caseDescription: "ติดต่อ OPD"
                        };
                        break;
                    }
                    default: {
                        return {
                            case: 0,
                            caseMessage: "-",
                            caseDescription: "-"
                        }
                        break;
                    }
                }
            }
        }
    }
}

