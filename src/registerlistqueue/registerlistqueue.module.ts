import { Module } from '@nestjs/common';
import { RegisterlistqueueController } from './registerlistqueue.controller';
import { RegisterlistqueueService } from './registerlistqueue.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SessionKeyService} from '../Session/SessionKeyService';
import { pmkpatient } from './queue.entity';

@Module({
  imports: [TypeOrmModule.forFeature([pmkpatient])],
  controllers: [RegisterlistqueueController],
  providers: [RegisterlistqueueService,SessionKeyService],
  exports: [RegisterlistqueueService,SessionKeyService]
})
export class RegisterlistqueueModule {}
