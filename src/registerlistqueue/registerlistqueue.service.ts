import { Injectable } from '@nestjs/common';
import { createConnection, getConnection, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { pmkpatient } from './queue.entity';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Repository } from 'typeorm';
@Injectable()
export class RegisterlistqueueService {
    constructor(
    @InjectRepository(pmkpatient)
    private readonly patientRespository: Repository<pmkpatient>) {

    }


    public  findpatient_queue(pid: string): Promise<any> {


        // const connection = createConnection({
        //     name: 'BOMCONNECT',
        //     type: 'postgres',
        //     host: '27.254.59.21',
        //     port: 5432,
        //     username: 'bomberman',
        //     password: 'P@ssw0rd12',
        //     database: 'registerQDB'
        // });

        //connection.then(x => x.connect());
        // const dta = connection.then(m => m.query(`select * from vw_patientqueue where idcard = $1`, [pid]);

        // connection.then(x => {
        //     if(x.isConnected == true)
        //     {
        //         x.close();
        //     }
        // });

        // console.log(dta);getConnection("BOMCONNECT").connect().then(m => m.query(`select * from vw_patientqueue where idcard = $1`, [pid])
        //return this.patientRespository.query(`select * from vw_patientcount where idcard = $1 and active='Y'`, [pid]);
        let wherecondition = pid.length==13?'idcard':(pid.includes('/')?'hn':(pid.match(/[A-Z]/)?'queueno':'refno'));
        return this.patientRespository.query(`select * from vw_patientcount where ${wherecondition} = $1 and active='Y'`, [pid]);

        //return connection.then(m => m.query(`select * from vw_patientqueue where idcard = $1`, [pid])).finally( );




    }

    public findpatientprocess(queueno: string): Promise<any> {
        let wherecondition = queueno.length==13?'idcard':(queueno.includes('/')?'hn':(queueno.match(/[A-Z]/)?'queueno':'refno'));
        console.log(`select * from vw_navigation where ${wherecondition} = ${queueno} and active='Y'`)
        return this.patientRespository.query(`select * from vw_navigation where ${wherecondition} = $1 and active='Y'`, [queueno]);
        //return this.patientRespository.query(`select * from vw_navigation where idcard = $1 and active='Y'`, [queueno]);

        //return connection.then(m => m.query(`select * from vw_patientqueue where idcard = $1`, [pid])).finally( );
    }


}
