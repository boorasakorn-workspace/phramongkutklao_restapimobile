import { Module } from '@nestjs/common';
import { CashierController } from './cashier/cashier.controller';
import { PharmacyController } from './pharmacy/pharmacy.controller';
import { PharcashService } from './pharcash.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SessionKeyPharcashService} from '../Session/SessionKeyPharcashService';
import { pharcash } from './pharcash.entity';

@Module({
  imports: [TypeOrmModule.forFeature([pharcash],'pharcash')],
  controllers: [CashierController, PharmacyController],
  providers: [PharcashService,SessionKeyPharcashService],
  exports: [PharcashService,SessionKeyPharcashService]
})
export class PharcashModule {}
