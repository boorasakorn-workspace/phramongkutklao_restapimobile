import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class pharcash {
    @PrimaryGeneratedColumn()
    uid: number;
   
    @Column({ nullable: true })
    pid: string;
   
    @Column({ nullable: true })
    hn: string;
    
    @Column({ nullable: true })
    firstName: string;
   
    @Column({ nullable: true })
    lastName: string;
   
    @Column({ nullable: true })
    fullName: string;
    
    @Column({ nullable: true })
    birthDate: string;
    
    @Column({ nullable: true })
    gender: string;
   
    @Column({ nullable: true })
    deceasedBoolean: string;

}