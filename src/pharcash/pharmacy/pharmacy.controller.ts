import { Controller, Post, Body, Delete, Put, Param, Query, Header, Req, Res, HttpStatus, Get } from '@nestjs/common';
import { PharcashService } from '../pharcash.service';
import { queuedata, activities, dataInstructions } from './queue';
import { SessionKeyPharcashService } from '../../Session/SessionKeyPharcashService';
@Controller('api/q/pharmacy')
export class PharmacyController {
    constructor(
        private readonly pharcashq: PharcashService,
        private readonly seesionkeey: SessionKeyPharcashService
    ) { }

    @Get()
    getpatientqueue(@Res() res, @Query() qr, @Param() pram) {
        let sesionkey = this.seesionkeey.extractSessionId(qr.sessionKey);
        let validate = this.seesionkeey.validateSession(sesionkey.signature, sesionkey.tokenId, 30);
        console.log(sesionkey);

        if (validate !== false) {
            let activities_: Array<activities> = [];

            let data: queuedata = new queuedata();
            let patientqueue;
            let dataInstructions_: Array<dataInstructions> = [];
            this.pharcashq.findpatientprocess(sesionkey.tokenId).then(xx => {
                xx.forEach(e => {
                    let dI: dataInstructions = new dataInstructions();
                    dI.IconPath = e.iconpath;
                    dI.title = e.description;
                    dataInstructions_.push(dI);
                });

            });
            this.pharcashq.findpatient_queue(sesionkey.tokenId).then(x => {
            
                if (x.length > 0) {

                    x.forEach(em => {
                        let ac: activities = new activities();
                        ac.qNumber = em.queueno;
                        ac.qType = "คิวการเงิน"
                        ac.id = em.patientdetailuid;
                        ac.serviceType = "pharmacy";
                        ac.location = em.queuelocation;
                        if(em.queueflag != 'Y'){
                            ac.case = 1;   
                        }else if(em.worklistuid17 != null){
                            ac.case = 2
                        }else if(em.worklistuid5 != null){
                            ac.case = 4
                        }else if(em.worklistuid16 != null){
                            ac.case = 5
                        }else if(em.worklistuid18 != null){
                            ac.case = 6
                        }else if(em.worklistuid2 != null){
                            ac.case = 7
                        }else if(em.worklistuid2 != null){
                            ac.case = 8
                        }else if(em.worklistuid9 != null){
                            ac.case = 9
                        }else{
                            ac.case = 3
                        }
                        ac.qBeforeSelf = em.countmeddication;
                        switch(ac.case){
                            case 1:ac.caseMessage = 'ยกเลิกคิว';
                                ac.caseDescription = 'ยกเลิกคิว';
                                ac.caseStatus = 'red';break;
                            case 2:ac.caseMessage = 'เสร็จสิ้น';
                                ac.caseDescription = 'เสร็จสิ้น';
                                ac.caseStatus = 'green';break;
                            case 3:ac.caseMessage = 'ยาค้างรับ';
                                ac.caseDescription = 'ยาค้างรับ';
                                ac.caseStatus = 'blue';break;
                            case 4:ac.caseMessage = 'ถึงคิวของคุณแล้ว';
                                ac.caseDescription = `ติดต่อรับยาที่ช่อง ${em.callcounter}`;
                                ac.caseStatus = 'green';break;
                            case 5:ac.caseMessage = 'รอจ่ายยา';
                                ac.caseDescription = 'รอจ่ายยา' + (ac.qBeforeSelf != null ? ` : รออีก ${ac.qBeforeSelf} คิว` : ``) + '(รอเรียกคิวรับยา)';
                                ac.caseStatus = 'yellow';break;
                            case 6:ac.caseMessage = 'ยามีปัญหา';
                                ac.caseDescription = 'ยามีปัญหา';
                                ac.caseStatus = 'red';break;
                            case 7:ac.caseMessage = 'กำลังจัดยา';
                                ac.caseDescription = 'กำลังจัดยา (รอเรียกคิวรับยา)';
                                ac.caseStatus = 'yellow';break;
                            case 8:ac.caseMessage = 'กำลังจัดยา';
                                ac.caseDescription = 'กำลังจัดยา (รอเรียกคิวการเงินก่อนรับยา)';
                                ac.caseStatus = 'yellow';break;
                            case 9:ac.caseMessage = 'รอจ่ายยา';
                                ac.caseDescription = 'รอจ่ายยา' + (ac.qBeforeSelf != null ? ` : รออีก ${ac.qBeforeSelf} คิว` : ``) + '(รอเรียกคิวการเงินก่อนรับยา)';
                                ac.caseStatus = 'yellow';break;
                        }                        
                        ac.qSubType = "ยาด่วน";
                        ac.channel = "16-20";                    
                        ac.serviceUnitName = "ห้องยา";
                        data.activities.push(ac);
                        data.hn = em.hn;
                        data.patientName = em.patientname;
                        data.pid = em.idcard;
                        ac.instructions = dataInstructions_;
                    });
                    res.status(HttpStatus.OK).send(data);
                }
                else {
                    res.status(HttpStatus.NOT_FOUND).send({ statuscode: HttpStatus.NOT_FOUND });
                }
            });
        }
        else {
            res.status(HttpStatus.UNAUTHORIZED).send({ statuscode: HttpStatus.UNAUTHORIZED });
        }
    }
}
