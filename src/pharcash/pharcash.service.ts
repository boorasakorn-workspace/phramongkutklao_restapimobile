import { Injectable } from '@nestjs/common';
import { createConnection, getConnection, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { pharcash } from './pharcash.entity';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Repository } from 'typeorm';
@Injectable()
export class PharcashService {
    constructor(
    @InjectRepository(pharcash,'pharcash')
    private readonly pharcashRespository: Repository<pharcash>) {

    }


    public  findpatient_queue(pid: string): Promise<any> {
        let wherecondition = pid.length==13?'idcard':(pid.includes('/')?'hn':'patientdetailuid');
        return this.pharcashRespository.query(`select * from vw_patientcount_all where ${wherecondition} = $1 and queueflag='Y'`, [pid]);

    }

    public findpatientprocess(queueno: string): Promise<any> {
        let wherecondition = queueno.length==13?'idcard':(queueno.includes('/')?'hn':'patientdetailuid');        
        return this.pharcashRespository.query(`select * from vw_patprocess where ${wherecondition} = $1 and statusflag='Y'`, [queueno]);
    }


}
